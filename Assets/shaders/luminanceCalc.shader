﻿Shader "own/luminanceCalc"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma only_renderers d3d11


			#define WINDOWSIZE 4
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			sampler2D _Mean;
			float4 offset[((WINDOWSIZE * 2 + 1) * (WINDOWSIZE * 2 + 1))];
			sampler2D _CameraDepthTexture;
			float _FullEffectDepth;
			float _DepthFilter;

			inline float getDepth(float2 uv) {
				float d = tex2D(_CameraDepthTexture, uv).r;
				return DECODE_EYEDEPTH(d);
			}

			float luminanceVariance(float2 uv)
			{
				int totalAmount = ((WINDOWSIZE * 2 + 1) * (WINDOWSIZE * 2 + 1));

				const fixed4 luminanceConverter = fixed4(0.3, 0.59, 0.11, 0);

				float variance = 0;
				float meanLuminance = dot(tex2D(_Mean, uv), luminanceConverter);

				int amount = 0;
				
				float depth = getDepth(uv);
				float ratio = clamp(depth / _FullEffectDepth, 0, 2.25);

				for (int k = 0; k < totalAmount; k++) {
					float2 coords = uv + offset[k] * ratio;
					float pointDepth = getDepth(coords);
					if (abs(pointDepth - depth) < _DepthFilter) {
						float luminance = dot(tex2D(_MainTex, coords), luminanceConverter);
						float deviation = luminance - meanLuminance;
						variance += deviation * deviation;
						amount++;
					}
				}
				
				if (amount > 0)
					variance /= amount;

				return variance;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				return luminanceVariance(i.uv);
			}
			ENDCG
		}
	}
}
