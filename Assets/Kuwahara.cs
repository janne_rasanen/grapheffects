﻿using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode]
public class Kuwahara : MonoBehaviour {
    public Shader kuwaharaShader;
    public Shader luminanceShader;
    public Shader medianShader;

    private Material kuwahara;
    private Material luminance;
    private Material median;

    public int windowSize = 4;
    public float pixelMultiplier = 0.5f;
    public float fullEffectDepth = 5f, depthFilter = 2f;

    private int oldWindowSize;
    private float oldPixelMultiplier, oldFullEffectDepth, oldDepthFilter;

    private Resolution r;

    void Start()
    {
        kuwahara = new Material(kuwaharaShader);
        luminance = new Material(luminanceShader);
        median = new Material(medianShader);
            
        kuwahara.hideFlags = HideFlags.HideAndDontSave;
        luminance.hideFlags = HideFlags.HideAndDontSave;
        median.hideFlags = HideFlags.HideAndDontSave;
    }

    void PreCalcOffsets()
    {
        Debug.Log("precalcing");
        r.width = Screen.width;
        r.height = Screen.height;
        oldWindowSize = windowSize;
        oldPixelMultiplier = pixelMultiplier;
        oldFullEffectDepth = fullEffectDepth;
        oldDepthFilter = depthFilter;
        int totalAmount = ((windowSize * 2 + 1) * (windowSize * 2 + 1));
        Vector4[] offset = new Vector4[totalAmount];
        int c = 0;

        float onePixelWidth = 1f / r.width * pixelMultiplier;
        float onePixelHeight = 1f / r.height * pixelMultiplier;

        for (int i = -windowSize; i <= windowSize; i++)
        {
            for (int j = -windowSize; j <= windowSize; j++)
            {
                offset[c++] = new Vector4(i * onePixelWidth, j * onePixelHeight, 0, 0);
            }
        }

        for (int i = 0; i < totalAmount; i++)
        {
            median.SetVector("offset" + i, offset[i]);
            luminance.SetVector("offset" + i, offset[i]);
        }

        float a = onePixelWidth / 2f * windowSize;
        float b = onePixelHeight / 2f * windowSize;
        kuwahara.SetVector("windowCenters0", new Vector4( a,  b, 0, 0));
        kuwahara.SetVector("windowCenters1", new Vector4( a, -b, 0, 0));
        kuwahara.SetVector("windowCenters2", new Vector4(-a,  b, 0, 0));
        kuwahara.SetVector("windowCenters3", new Vector4(-a, -b, 0, 0));

        median.SetInt("offsetAmount", totalAmount);
        // luminance.SetInt("totalAmount", totalAmount);

        median.SetFloat("_FullEffectDepth", fullEffectDepth);
        luminance.SetFloat("_FullEffectDepth", fullEffectDepth);
        kuwahara.SetFloat("_FullEffectDepth", fullEffectDepth);

        median.SetFloat("_DepthFilter", depthFilter);
        luminance.SetFloat("_DepthFilter", depthFilter);
        kuwahara.SetFloat("_DepthFilter", depthFilter);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        // RenderMean(src, dest);
        // RenderLuminance(src, dest);
        RenderKuwarahara(src, dest);
    }

    private void RenderLuminance(RenderTexture src, RenderTexture dest)
    {
        RenderTexture medianTex = RenderTexture.GetTemporary(src.width, src.height);
        Graphics.Blit(src, medianTex, median);
        luminance.SetTexture("_Mean", medianTex);
        Graphics.Blit(src, dest, luminance);
        RenderTexture.ReleaseTemporary(medianTex);
    }

    private void RenderMean(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, median);
    }

    private void RenderKuwarahara(RenderTexture src, RenderTexture dest)
    {
        RenderTexture luminanceVarianceTex = RenderTexture.GetTemporary(src.width, src.height);
        RenderTexture medianTex = RenderTexture.GetTemporary(src.width, src.height);
        Graphics.Blit(src, medianTex, median);
        luminance.SetTexture("_Mean", medianTex);
        Graphics.Blit(src, luminanceVarianceTex, luminance);

        kuwahara.SetTexture("_Luminance", luminanceVarianceTex);
        kuwahara.SetTexture("_Mean", medianTex);
        Graphics.Blit(src, dest, kuwahara);

        RenderTexture.ReleaseTemporary(luminanceVarianceTex);
        RenderTexture.ReleaseTemporary(medianTex);
    }

    void Update()
    {
        if (r.width != Screen.width || r.height != Screen.height ||
            oldPixelMultiplier != pixelMultiplier || oldWindowSize != windowSize ||
            oldFullEffectDepth != fullEffectDepth || oldDepthFilter != depthFilter
            )
        {
            PreCalcOffsets();
        }
    }
}
