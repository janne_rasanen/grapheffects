﻿Shader "own/kuwahara"
{
	Properties
	{
		
	} 
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			// #pragma only_renderers d3d11

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _Luminance;
			sampler2D _Mean;

			sampler2D _CameraDepthTexture;
			
			float4 windowCenters[4];
			float _DepthCutoff;
			float _FullEffectDepth;
			float _DepthFilter;

			inline float getDepth(float2 uv) {
				float d = tex2D(_CameraDepthTexture, uv).r;
				return DECODE_EYEDEPTH(d);
			}

			fixed4 frag (v2f input) : SV_Target
			{
				float info[4];
				
				const fixed4 luminanceConverter = fixed4(0.3, 0.59, 0.11, 0);

				float depth = getDepth(input.uv);
				float ratio = clamp(depth / _FullEffectDepth, 0, 2.25);

				bool found = false;

				for (int i = 0; i < 4; i++) {
					float2 coords = input.uv + windowCenters[i] * ratio;
					float pointDepth = getDepth(coords);

					if (abs(pointDepth - depth) < _DepthFilter) {
						info[i] = dot(tex2D(_Luminance, coords), luminanceConverter);
						found = true;
					}
					else {
						info[i] = 10000;
					}
					
				}

				float2 p = input.uv;

				if (found) {
					int smallestIndex = 0;

					for (int j = 1; j < 4; j++) {
						if (info[j] < info[smallestIndex])
							smallestIndex = j;
					}
					p += windowCenters[smallestIndex];
				}

				return tex2D(_Mean, p);
			}
			ENDCG
		}
	}
}
