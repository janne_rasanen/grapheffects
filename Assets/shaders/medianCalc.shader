﻿Shader "own/median"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma only_renderers d3d11

			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float _FullEffectDepth;
			float _DepthFilter;

			float4 offset[961];
			int offsetAmount;

			inline float getDepth(float2 uv) {
				float d = tex2D(_CameraDepthTexture, uv).r;
				return DECODE_EYEDEPTH(d);
			}
			
			fixed4 frag (v2f input) : SV_Target
			{
				fixed4 mean = fixed4(0, 0, 0, 0);
			
				float depth = getDepth(input.uv);
				float ratio = clamp(depth / _FullEffectDepth, 0, 2.25);

				int amount = 0;
				for (int k = 0; k < offsetAmount; k++) {
					float2 coords = input.uv + offset[k] * ratio;
					float pointDepth = getDepth(coords);

					if (abs(pointDepth - depth) < _DepthFilter) {
						mean += tex2D(_MainTex, coords);
						amount++;
					}
				}

				if (amount > 0)
					mean /= amount;

				return mean;
			}
			ENDCG
		}
	}
}
