﻿using UnityEngine;
using System.Collections;

public class CamControls : MonoBehaviour {

    public float rotationVelocity = 1f;
    public float zoomVelocity = 0.1f;

    private Transform camT;
    

    void Start()
    {
        camT = GetComponentInChildren<Camera>().transform;
    }

	void Update () {
        RotateCamera();
        ZoomCamera();
    }

    private void RotateCamera()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 r = transform.localEulerAngles;
        if (horizontal < 0)
        {
            r.y += rotationVelocity;
        }
        else if (horizontal > 0)
        {
            r.y -= rotationVelocity;
        }

        float f = r.x;
        if (vertical > 0)
        {
            f += rotationVelocity;
        }
        else if (vertical < 0)
        {
            f -= rotationVelocity;
        }
        if (f < 0)
            f += 360f;
        if (f > 360f)
            f -= 360f;

        if (f < 60 && f > 30)
            r.x = f;

        transform.localEulerAngles = r;
    }

    private void ZoomCamera()
    {

        if (Input.GetKey(KeyCode.Q))
        {
            Zoom(1f);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            Zoom(-1f);
        }
    }

    private void Zoom(float factor)
    {
        Vector3 v = camT.localPosition;
        float newValue = factor * zoomVelocity + v.z;
        newValue = Mathf.Clamp(newValue, -3f, -1f);
        v.z = newValue;
        camT.localPosition = v;
    }
}
