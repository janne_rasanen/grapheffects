﻿Shader "own/kuwahara2"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Luminance("Texture", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf Lambert
	struct Input {
		float4 color : COLOR;
	};
	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = half4(1, 0, 0, 1);
	}
	ENDCG
	}
		Fallback "Diffuse"
}
